from scipy.cluster import hierarchy
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn import metrics
d=[]
f = open("b.dat")
for l in f:
  l = l.replace("\n", "")
  d.append([float(x) for x in l.split()])
d = StandardScaler().fit_transform(d)
model = AgglomerativeClustering(linkage='ward', n_clusters=3)
model.fit(d)
pred = model.fit_predict(d)
plt.figure(figsize=(12, 10))
ans = model.predict(d[:,0])
plt.scatter(d[:,0], d[:,1], c=pred, cmap='rainbow', alpha=0.9)
labels = model.labels_
print(accuracy_score())
# den = hierarchy.dendrogram(hierarchy.linkage(d, method='ward'))
plt.show()

import pickle
file_ = open("test2.pkl", 'wb')
pickle.dump(model, file_)
file_.close()