from sklearn.metrics import accuracy_score
import pickle
file_ = np.loadtxt("b_test.dat")
x = file_[:,:-1].astype(float)
y = file_[:,-1].astype(float)
fl = open("test2.pkl", 'rb')
model2 = pickle.load(file_)
file_.close()
ans = model2.predict(x)
print(accuracy_score(y, ans))